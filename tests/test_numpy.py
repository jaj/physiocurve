import pytest
from physiocurve import ECG, PPG, Flow, Pressure

from .common import load_data


@pytest.fixture
def data():
    pd_data = load_data()
    d = {k: s.to_numpy() for k, s in pd_data.items()}
    ppg = PPG(d["ppg"])
    vel = Flow(d["vel"])
    ecg = ECG(d["ecg"])
    art = Pressure(d["art"])
    return [art, ppg, ecg, vel]


def run_tests(data):
    art, ppg, ecg, vel = data

    _ = ppg.argfeet
    _ = ppg.argdia
    _ = ppg.argdic
    _ = ppg.argsys
    _ = ppg.heartrate
    _ = ppg.nra

    _ = vel.argstarts
    _ = vel.argstops

    _ = ecg.argpwave
    _ = ecg.argqwave
    _ = ecg.argrwave
    _ = ecg.argswave
    _ = ecg.argtwave
    _ = ecg.heartrate

    _ = art.argfeet
    _ = art.argdia
    _ = art.argdic
    _ = art.argsys
    _ = art.heartrate
    _ = art.means
    _ = art.nra
    _ = art.sqi
