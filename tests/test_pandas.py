import pytest
from physiocurve.pandas import ECG, PPG, Flow, Pressure

from .common import load_data


@pytest.fixture
def data():
    d = load_data()
    ppg = PPG(d["ppg"])
    vel = Flow(d["vel"])
    ecg = ECG(d["ecg"])
    art = Pressure(d["art"])
    return [art, ppg, ecg, vel]


def test_errors(data):
    art, ppg, ecg, vel = data

    _ = ppg.samplerate
    print("ppg.feet", ppg.feet.shape)
    print("ppg.diastolics", ppg.diastolics.shape)
    print("ppg.dicrotics", ppg.dicrotics.shape)
    print("ppg.systolics", ppg.systolics.shape)
    print("ppg.tanfeet", ppg.tanfeet.shape)
    print("ppg.heartrate", ppg.heartrate.shape)
    print("ppg.means", ppg.means.shape)
    print("ppg.nra", ppg.nra.shape)
    assert ppg.diastolics.shape == ppg.systolics.shape

    _ = vel.samplerate
    print("vel.starts", vel.starts.shape)
    print("vel.stops", vel.stops.shape)

    _ = ecg.samplerate
    print("ecg.pwaves", ecg.pwaves.shape)
    print("ecg.qwaves", ecg.qwaves.shape)
    print("ecg.rwaves", ecg.rwaves.shape)
    print("ecg.swaves", ecg.swaves.shape)
    print("ecg.twaves", ecg.twaves.shape)
    print("ecg.heartrate", ecg.heartrate.shape)

    _ = art.samplerate
    print("art.feet", art.feet.shape)
    print("art.diastolics", art.diastolics.shape)
    print("art.dicrotics", art.dicrotics.shape)
    print("art.systolics", art.systolics.shape)
    print("art.tanfeet", art.tanfeet.shape)
    print("art.heartrate", art.heartrate.shape)
    print("art.means", art.means.shape)
    print("art.nra", art.nra.shape)
    print("art.sqi", art.sqi.shape)
    assert art.diastolics.shape == art.systolics.shape
