from io import BytesIO

import pandas as pd

from .testdata import data


def load_data():
    df = pd.read_pickle(BytesIO(data))
    ppg = df["Pleth-PLETH(-)"].dropna()
    vel = df["Wave 1-Vel(NOS)"].dropna()
    ecg = df["II-II(mV)"].dropna()
    art = df["ART-ART(mmHg)"].dropna()
    return {"ppg": ppg, "vel": vel, "ecg": ecg, "art": art}
