from physiocurve.common import diagnostics
from physiocurve.pandas import ECG, Pressure


def plot_ecg(d):
    import matplotlib.pyplot as plt

    ecg = ECG(d["ppg"])
    plt.plot(ecg._series.index, ecg._series.to_numpy())
    plt.plot(ecg._series.index, ecg._values, c="k")
    plt.scatter(ecg.pwaves.index, ecg.pwaves.to_numpy(), c="r")
    plt.scatter(ecg.rwaves.index, ecg.rwaves.to_numpy(), c="g")
    plt.scatter(ecg.twaves.index, ecg.twaves.to_numpy(), c="y")
    plt.show()


def plot_feet(d):
    import matplotlib.pyplot as plt
    import numpy as np

    p = Pressure(d["art"])
    plt.plot(p._values)
    plt.scatter(p.argfeet, p.feet.to_numpy(), c="r", label="deriv")
    plt.scatter(p.argtanfeet, p.tanfeet.to_numpy(), c="g", label="tangent")
    plt.legend()
    _ = p.tanfeet
    # diagnostics['tangents'].append([diaidx, sysidx, diay, foot_x, slope, tangent_b])
    for t in diagnostics["tangents"]:
        beg_interval, end_interval, diay, foot_x, slope, tangent_b = t
        # Plot horizontal tangent
        hor_x = np.arange(beg_interval, foot_x + 1, step=1, dtype=np.int64)
        hor_y = np.full(hor_x.shape, diay)
        plt.plot(hor_x, hor_y, "k--")

        # Plot slope tangent
        slope_x = np.arange(foot_x, end_interval + 1, step=1, dtype=np.int64)
        slope_y = slope * slope_x + tangent_b
        plt.plot(slope_x, slope_y, "k--")

        # Plot foot projection
        proj_y = np.arange(diay, diay + 5, step=1)
        proj_x = np.full(proj_y.shape, foot_x)
        plt.plot(proj_x, proj_y, "k--")
    plt.show()
